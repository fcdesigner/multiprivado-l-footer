<?php
    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $page = end($link_array);

	$is_livestream = ($page === "live-stream") ? 1 : 0;
	$language = $_SESSION['livemode_multiplier']['language'];
?>

<?php if(isset($formdata) && !empty($formdata)){ ?>
	<div class="module-small bg-dark" id = "footer_content">
	<div class="container">
		<div class="row">
		<div class="widget">
			<h5 class="widget-title font-alt">&nbsp;&nbsp;&nbsp;<?php echo ($formdata->footer_title)?$formdata->footer_title:$this->lang->line('footer_title');?></h5>
			<p>&nbsp;&nbsp;&nbsp;<?php echo ($formdata->footer_sub_title)?$formdata->footer_sub_title:$this->lang->line('footer_sub_title');?></p>
			<?php  if($formdata->company_email){?>
				<p>&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('contact');?>:&nbsp;<a href="mailto:<?php echo $formdata->company_email;?>"><?php echo $formdata->company_email;?></a></p>
			<?php }?>
			
			<?php if ($is_livestream){ ?>
				<h3 id = "gallery_name" > Gallery </h3>
				<div class = "row" id = "video_gallery"></div>
			<?php } ?>

		</div>
		</div>
	</div>
	</div>


	<hr class="divider-d">
	<footer class="footer bg-dark">
	<div class="container">
		<div class="row">
		<div class="col-sm-6">
			<p class="copyright font-alt">&copy; 2018&nbsp;<a href="#"><?php echo ($formdata->company_name)?$formdata->company_name:$this->lang->line('company');?></a>, <?php echo $this->lang->line('rights');?></p>
		</div>
		<br>
		<br>
		<div class="col-sm-6">
			<div class="footer-social-links">
					<?php  if($formdata->facebook_url){?>
						<a href="<?php echo addhttp($formdata->facebook_url); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
					<?php }?>
					<?php  if($formdata->twitter_url){?>
						<a href="<?php echo addhttp($formdata->twitter_url); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
					<?php }?>
					<?php  if($formdata->website_url){?>
						<a href="<?php echo addhttp($formdata->website_url); ?>" target="_blank"><i class="fa fa-dribbble"></i></a>
					<?php }?>
					<?php  if($formdata->skype_url){?>
						<a href="skype:<?php echo $formdata->skype_url; ?>?chat" ><i class="fa fa-skype"></i></a>
					<?php }?>
					<?php  if($formdata->youtube_url){?>
						<a href="<?php echo addhttp($formdata->youtube_url); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
					<?php }?>
					<?php  if($formdata->linkedIn_url){?>
						<a href="<?php echo addhttp($formdata->linkedIn_url); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
					<?php }?>
				</div>
		</div>
		</div>
	</div>
	</footer>
	</div>
<?php }?>
<div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
<!--
JavaScripts
=============================================
-->

<?php $var = "3.6.3" ?>
<script src='<?php echo base_url("assets/lib/jquery/dist/jquery.js"); ?>'></script>
<script src="<?php echo base_url("assets/newTheme/assets/js/vendors/vendors.js?v=").$var ?>"></script>
<!-- Local Revolution tools-->
<!-- Only for local and can be removed on server-->

<script src="<?php echo base_url("assets/newTheme/assets/js/custom.js?v=").$var ?>"></script>

<script src="<?php echo base_url("assets/newTheme/assets/js/jquery.playSound.js?v=").$var ?>"></script>

<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/twemoji/2/twemoji.min.js?v=").$var ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/twemoji-picker.js?v=").$var ?>"></script>

<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay.js?v=").$var ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay_progress.js?v=").$var ?>"></script>

<script src="<?php echo base_url("assets/newTheme/assets/js/magicsuggest.js?v=").$var ?>"></script>
<script src="<?php echo base_url("assets/newTheme/assets/js/perfect-scrollbar.jquery.min.js?v=").$var ?>"></script>
<script src='<?php echo base_url("assets/lib/bootstrap/dist/js/bootstrap.min.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/wow/dist/wow.js"); ?>'></script>
<script src='<?php echo base_url("assets/js/parsley.min.js"); ?>'></script>
<?php if(getClientSessionVariable('language')=='spanish'){ ?>
	<script src='<?php echo base_url("assets/js/i18n/es.js"); ?>'></script>
<?php }?>
<?php if(getClientSessionVariable('language')=='portuguese'){ ?>
	<script src='<?php echo base_url("assets/js/i18n/pt-br.js"); ?>'></script>
<?php }?>
<script src='<?php echo base_url("assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/isotope/dist/isotope.pkgd.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/imagesloaded/imagesloaded.pkgd.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/flexslider/jquery.flexslider.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/owl.carousel/dist/owl.carousel.min.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/smoothscroll.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/magnific-popup/dist/jquery.magnific-popup.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"); ?>'></script>
<script src='<?php echo base_url("assets/js/plugins.js"); ?>'></script>
<script src='<?php echo base_url("assets/js/main.js"); ?>'></script>


<?php if($is_livestream){ ?>

	<script>

		/** Gallery JS */

		//Temporary vars for manual translation
		var language = "<?= $language ?>";

		var translation = {
			seeMore: {
				portuguese: "Ver Mais",
				english: "See More",
				spanish: "Ver Más",
			},
			seeLess: {
				portuguese: "Ver Menos",
				english: "See Less",
				spanish: "Ver Menos",
			},
			gallery: {
				portuguese: "Galeria",
				english: "Gallery",
				spanish: "Galería",
			},
			watched: {
				portuguese: "Assistido",
				english: "Watched",
				spanish: "Visto",
			},
		};

		// In the future we will recive this from the backend. This var would recive the first 9 or so videos with source, title, description, date, author and so on
		var gallery_itens = [];

		// Default value for the quantity of new loaded videos when starting
		var load_quantity_start = 9;

		// Default value for the quantity of new loaded videos when ending the scroll
		var load_quantity_end_scroll = 6;

		//Tells if there are no more itens to be loaded
		var no_more_itens = false;

		$(document).ready(function(){

			$("#gallery_name").text(translation.gallery[language]);

			addNewVideosToGallery();


			$("#video_gallery").scroll(function () {

				var element = document.getElementById("video_gallery");

				if( scrolledToBottom(element) && gallery_itens.length === 0 && !no_more_itens){
					addLoader();
					addNewVideosToGallery();
				}

			});

		});

		$(document).on('click', '.description-see-more', function(e){
			$(this).text(translation.seeLess[language]).removeClass("description-see-more").addClass("description-see-less").parent().children(".item-description").removeClass("truncate-text");
			e.preventDefault();
		});

		$(document).on('click', '.description-see-less', function(e){
			$(this).text(translation.seeMore[language]).removeClass("description-see-less").addClass("description-see-more").parent().children(".item-description").addClass("truncate-text");
			e.preventDefault();
		});

		/**
		 * Load the itens in the gallery
		 */
		function loadGalleryItems(load_quantity){

			/**
			 * AJAX CALL GOES HERE
			 */
			
			// for(i = 0; i < 9 ;i++){
			// 	gallery_itens.push( static_itens.shift() );
			// }
			
			gallery_itens = static_itens.splice(0, 9);

			if(static_itens.length === 0){
				no_more_itens = true;
			}
		}

		/**
		 * Add a new video to the end of the gallery
		 */
		function addVideo(){

			var item = gallery_itens.shift();

			$("#video_gallery").append(`
				<div class = "gallery-item" data-id = ${item.id}>

					<video controls>
						<source src = ${item.source}>
					</video>

					<div class = "item-information">
						<h4 class = "item-title truncate-text" title = "${item.title}"> ${item.title} </h4>
						<span class = "item-author item-metadata"> ${item.author} </span>
						<div class = "item-description-wrapper">
							<span class = "item-description item-metadata truncate-text"> ${item.description} </span>
							<a class = "description-see-more" type = "button" href = "#">${translation.seeMore[language]}</a>
						</div>
						<span class = "item-date item-metadata"> ${item.date} </span>
						<span class = "item-watched-hours item-metadata"> ${item.watched_hours}h ${translation.watched[language]} </span>
						<hr class = "sm-divider">
					</div>

				</div>
			`);

		}

		/**
		 * Add news videos to the gallery when you reach the end of the scroll
		 */
		function addNewVideosToGallery(){

			loadGalleryItems(load_quantity_end_scroll);

			setTimeout(function(){

				$.each(gallery_itens, function(index, value){ 
					addVideo();
				});

				$(".item-title").tooltip()
				removeLoader();
			}, 1500);
			
		}

		/**
		 * Checks if the element has been scrolled to the end
		 */
		function scrolledToBottom(element){
			return (element.offsetHeight + element.scrollTop >= element.scrollHeight - 20) ? true : false;
		}

		/**
		 * Add the loader 
		 */
		function addLoader(target){
			$("#video_gallery").append(`<div id="loader"></div>`);
		}

		/**
		 * Remove the loader
		 */
		function removeLoader(){
			$("#loader").remove();
		}

		var static_itens = [
			{
				id: "1",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/31-03%20%E2%80%93%20Ponto%20de%20apoio%20Itaipava.mp4",
				title: "Ponto de apoio",
				author: "Prefeitura de Petrópolis",
				description: "🔺️ O ponto de apoio para pacientes suspeitos de coronavírus já está funcionando em Itaipava. A unidade fica em frente ao Terminal Itaipava e funciona 24h. O médico Vitor Eiras Antunes mostrou todo o espaço. #PetropolisNoCombateAoCoronavirus #EscolhaAVida",
				date: "31 de março de 2020 às 13:47",
				watched_hours: "01:00",
			},
			{
				id: "2",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/31-03%20-%20Drive-Thru%20dos%20Idosos.mp4",
				title: "Drive-Thru dos Idosos",
				author: "Prefeitura de Petrópolis",
				description: "🔺️ Começou hoje a vacinação contra a gripe para os idosos por Drive-Thru. Esta fase atende pessoas entre 60 e 69 anos até às 15h. A ação também acontece no Parque Municipal, em Itaipava. #PetropolisNoCombateAoCoronavirus #EscolhaAVida",
				date: "31 de março de 2020 às 10:10",
				watched_hours: "01:00",
			},	
			{
				id: "3",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/29-03%20%E2%80%93%20Agita%20Petr%C3%B3polis.mp4",
				title: "Agita Petrópolis ²",
				author: "Prefeitura de Petrópolis",
				description: "🎶 Ahhh bateu aquela preguiça? Não, não!! Trazemos um combo de exercícios, com 3 professoras do projeto Agita Petrópolis: Dani, Anna e Beatriz irão mostrar como é possível praticar atividades físicas durante a quarentena! Vamos lá galera!!!!! 🏃‍♀️🏃‍♂️🏃‍♀️🏃‍♂️🏃‍♀️",
				date: "29 de março de 2020 às 16:52",
				watched_hours: "01:00",
			},	
			{
				id: "4",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/28-03%20%E2%80%93%20Entrega%20de%20alimentos%20CEI.mp4",
				title: "Entrega de alimentos CEI",
				author: "Prefeitura de Petrópolis",
				description: "A entrega de alimentos não perecíveis para as famílias dos alunos matriculados nos Centros de Educação Infantil da rede municipal de Educação teve continuidade nesta sexta-feira (27.03), no CEI Jorge Rolando, no Morin. A expectativa é de que mais de seis mil famílias recebam os produtos que foram selecionados para o reforço alimentar das crianças de até 5 anos. A Secretaria de Educação estuda ampliar a ação para toda a rede. Cristiane Hang, mãe da Gabriela de 2 anos, aprovou a iniciativa.Confira!",
				date: "28 de março de 2020 às 18:00",
				watched_hours: "01:00",
			},
			{
				id: "5",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/27-03%20%E2%80%93%20Remessa%20Equipamentos%20de%20Prote%C3%A7%C3%A3o.mp4",
				title: "Remessa Equipamentos de Proteção",
				author: "Prefeitura de Petrópolis",
				description: "O município recebeu nesta sexta-feira (27.03) mais uma remessa de Equipamentos de Proteção Individual (EPIs), que está sendo utilizado principalmente para os servidores da saúde que estão atuando na linha de frente no combate ao coronavírus. A Superintendente de Atenção à Saúde, Fátima Coelho falou da importância deste material. <br> Confira um dos produtos da remessa de hoje: <br> 1 milhão e 200 mil luvas <br> 20 mil aventais <br> 350 mil máscaras sanfonadas <br> 7 mil máscaras N95",
				date: "27 de março de 2020 às 15:14",
				watched_hours: "01:00",
			},
			{
				id: "6",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/26-03%20%E2%80%93%20Controle%20sanit%C3%A1rio%20Quitandinha.mp4",
				title: "Controle sanitário Quitandinha",
				author: "Prefeitura de Petrópolis",
				description: "O movimento do Controle Sanitário no pórtico do Quitandinha segue menor na tarde desta quinta-feira, e as atividades continuam assim como nas outras entradas da cidade. #PetropolisNoCombateAoCoronaVirus #EscolhaAVida #PetropolisEMais #MaisPrevencao",
				date: "26 de março de 2020 às 15:57",
				watched_hours: "01:00",
			},
			{
				id: "7",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/26-03%20%E2%80%93%20Quentinha%20Restaurante%20Popular.mp4",
				title: "Quentinha Restaurante Popular",
				author: "Prefeitura de Petrópolis",
				description: "🔺️ A coordenadora do Restaurante Popular, Débora da Cruz contou como a nova configuração do Restaurante Popular. <br> ➡️As mil refeições diárias do Restaurante Popular já estão sendo entregues com a opção de levar também os lanches de café da manhã e da tarde, todas de uma única vez. <br> ➡️Agora, o Restaurante Popular funciona a partir de 10h e fica aberto até o fim da distribuição das mil refeições diárias. As pessoas podem retirar, na porta do local, os kits com o almoço – com a quentinha, uma fruta ou doce – e os cafés da manhã e da tarde, com um pão e uma fruta cada. #PetropolisNoCombateAoCoronavirus #EscolhaAVida",
				date: "26 de março de 2020 às 13:11",
				watched_hours: "01:00",
			},
			{
				id: "0",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/24-03%20%E2%80%93%20Em%20Alta_1.mp4",
				title: "24 de Março em Alta",
				author: "Prefeitura de Petrópolis",
				description: "Jornal 24 de março",
				date: "24 de março de 2020 às 13:47",
				watched_hours: "01:00",
			},
			{
				id: "8",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/23-03%20%E2%80%93%20Dias%20e%20Hor%C3%A1rios%20do%20Drive-Thru%20%28Prefeito%29.mp4",
				title: "Dias e Horários do Drive-Thru (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "🔺️ Atenção aos dias é horários do serviço de vacinação para idoso Drive no Thru. #PetropolisNoCombateAoCoronavirus #EscolhaAVida",
				date: "23 de março de 2020 às 12:36",
				watched_hours: "01:00",
			},
			{
				id: "9",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/23-03%20%E2%80%93%20Ruas%20lavadas%20para%20conter%20o%20coronav%C3%ADrus.mp4",
				title: "Ruas lavadas para conter o coronavírus",
				author: "Prefeitura de Petrópolis",
				description: "A Prefeitura de Petrópolis está agindo rápido para conter o vírus COVID-19 na cidade. As ruas Aureliano Coutinho e Teresa, foram lavadas esta noite. Precisamos da ajuda da população nesse momento. Cumpra nossos decretos e siga nossas recomendações. Vamos escolher a vida!!! #PetrópolisEMais #PetropolisNoCombateaocoronavírus #EscolhaAVida",
				date: "23 de março de 2020 às 08:33",
				watched_hours: "01:00",
			},
			{
				id: "10",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/22-03%20%E2%80%93%20Video%20Pr%C3%A9%20Drive-Thru%20%28Prefeito%29.mp4",
				title: "Video Pré Drive-Thru (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "Atenção!!! Começa amanhã a campanha de vacinação contra a gripe para os idosos. Por conta do combate ao coronavírus, a Prefeitura de Petrópolis montou um cronograma especial para atender esse público. A vacinação atravésdo Drive Thru terá início na quarta-feira, 25/03. Confira no vídeo como ficou a programação! #PetropolisEMais #MaisSaude",
				date: "22 de março de 2020 às 19:00",
				watched_hours: "01:00",
			},
			{
				id: "11",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/21-03%20%E2%80%93%20Decreto%20Fechamento%20do%20Com%C3%A9rcio%20%28Prefeito%29-2.mp4",
				title: "Decreto Fechamento do Comércio (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "🔺️ Prefeito Bernardo Rossi decretou agora pela manhã, a partir de segunda-feira todo o comércio da cidade, entre eles shoppings, inclusive dos distritos deverá ficar fechado, exceto supermercado padarias, postos de combustíveis e farmácias. O município está tomando medidas mais severas com o objetivo de combater a propagação do coronavírus. O novo decreto estará disponível no site da prefeitura. <br>Vale lembrar que os estabelecimentos que têm autorização para funcionar por necessidade pública, os proprietários devem garantir a segurança dos funcionários seguindo as orientações de prevenção. <br>⚠️Foi decidido também que os funcionários da Comdep e manutenção viária da Secretaria de Obras, com mais de 60 anos, terão as férias antecipadas e deverão ficar em casa nos próximos 15 dias. Todos os funcionários da SinalPark - empresa que administra o estacionamento rotativo também ficarão em casa. <br>É muito importante que todos sigam as orientações ficando em casa. Juntos escolhendo pela vida! #PetropolisNoCombateAoCoronavirus #EscolhaAVida #PetropolisEMais #MaisPrevencao",
				date: "21 de março de 2020 às 10:37",
				watched_hours: "01:00",
			},
			{
				id: "12",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/20-03%20%E2%80%93%20Mais%20um%20caso%20confirmado%20de%20coronav%C3%ADrus.mp4",
				title: "Mais um caso de coronavírus confirmado",
				author: "Prefeitura de Petrópolis",
				description: "Município confirma mais um caso de coronavírus <br> <br> A secretária de Saúde, Fabíola Heck atualizou o Boletim epidemiológico. Agora são 36 casos suspeitos, 10 foram descartados pela Secretaria Estadual de Saúde , 24 em análise e 2 confirmado. O município já trabalha com a prevenção e conta com a colaboração da população que fique em casa e juntos vamos combater a propagação do vírus em nossa cidade. #PetropolisNoCombateAoCoronaVirus #PetropolisEMais #MaisPrevencao #MaisSaude",
				date: "20 de março de 2020 às 13:15",
				watched_hours: "01:00",
			},
			{
				id: "13",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/20-03%20%E2%80%93%20Live%20Decreto%20de%20bares%20e%20restaurantes%20%28Prefeito%29.mp4",
				title: "Live Decreto de bares e restaurantes (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "Durante uma reunião com autoridades de segurança do município, foi definido que bares ficarão fechados a partir de hoje e restaurantes e lanchonetes poderão funcionar até 16h. #PetropolisNoCombateAoCoronaVirus #PetropolisEMais #MaisPrevencao",
				date: "20 de março de 2020 às 10:20",
				watched_hours: "01:00",
			},
			{
				id: "14",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/20-03%20%E2%80%93%20Campanha%20oficial%20preven%C3%A7%C3%A3o%20ao%20coronav%C3%ADrus%20%28Prefeito%29.mp4",
				title: "Campanha oficial prevenção ao coronavírus (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "O município lançou a campanha oficial de prevenção ao coronavírus em Petrópolis. Estamos juntos trabalhando para amenizar a propagação do vírus e garantir a saúde da população. Convidamos dois especialistas para esclarecer algumas dúvidas. Contamos com a sua ajuda nesta campanha! #PetropolisEMais #MaisSaude #Petropolisnocombateaocoronavírus #EscolhaAVida",
				date: "20 de março de 2020 às 07:37",
				watched_hours: "01:00",
			},
			{
				id: "15",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/19-03%20%E2%80%93%20Primeiro%20caso%20confirmado%20coronav%C3%ADrus%20%28Prefeito%29.mp4",
				title: "Primeiro caso confirmado coronavírus (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "Prefeito anunciou o primeiro caso confirmado de coronavírus na cidade. Reforçamos que o município já está agindo com várias medidas de prevenção e contamos que toda a população siga as orientações para combater a propagação do vírus. #petropolisnocombateaocoronavirus #escolhaavida #petropolisEMais #MaisPrevencao",
				date: "19 de março de 2020 às 11:12",
				watched_hours: "01:00",
			},
			{
				id: "16",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/18-03%20%E2%80%93%20Live%20a%C3%A7%C3%B5es%20para%20o%20funcionamento%20do%20com%C3%A9rcio%20%28Prefeito%29.mp4",
				title: "Live ações para o funcionamento do comércio (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "A Prefeitura de Petrópolis define novas ações para o funcionamento do comércio na cidade. Durante a reunião com os representantes do segmento, foi definido que as lojas da Rua Teresa e Bingen irão funcionar das 10h às 16h e as lojas do Centro Histórico das 12h às 18h. A Feira Livre será reduzida em 50% e a Feirinha de Itaipava ficará fechada. A ação não vale para farmácias e supermercados. O objetivo é diminuir o fluxo das pessoas, prevenindo a propagação do coronavírus. #PetropolisEMais #MaisSaude #Todoscontraocoronavírus",
				date: "18 de março de 2020 às 17:25",
				watched_hours: "01:00",
			},
			{
				id: "17",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/17-03%20%E2%80%93%20Orienta%C3%A7%C3%A3o%20para%20as%20academias%20na%20preven%C3%A7%C3%A3o.mp4",
				title: "Orientação para as academias na prevenção",
				author: "Prefeitura de Petrópolis",
				description: "O município se reuniu hoje com representantes das academias para passar as orientações que o governo está realizando na prevenção do coronavírus. <br> Conversamos com o vice-presidente do Conselho Regional de Educação Física, André Fernandes. <br> #PetropolisEMais #MaisPrevençao #MaisSaude",
				date: "18 de março de 2020 às 17:41",
				watched_hours: "01:00",
			},
			{
				id: "18",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/17-03%20%E2%80%93%20Segunda%20Live%20coletiva%20de%20imprensa%20para%20novas%20medidas%20de%20preven%C3%A7%C3%A3o%20ao%20coronav%C3%ADrus%20%28Prefeito%29.mp4",
				title: "Segunda Live coletiva de imprensa para novas medidas de prevenção ao coronavírus (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "Coletiva de imprensa para novas medidas de prevenção do coronavírus. #prefeiturapetropolisb #PetropolisEMais #MaisPrevencao",
				date: "17 de março de 2020 às 12:02",
				watched_hours: "01:00",
			},
			{
				id: "19",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/16-03%20%E2%80%93%20Assinatura%20dos%20contratos%20Vicenzo%20Rivetti.mp4",
				title: "Assinatura dos contratos Vicenzo Rivetti",
				author: "Prefeitura de Petrópolis",
				description: "Começou a assitura dos contratos do conjunto habitacional Vicenzo Rivetti. No dia do aniversário de Petrópolis, a comemoração é dos novos moradores do condomínio. Rosana Guimarães, uma das primeiras proprietárias a receber a doumentação não segurou a emoção. #PrefeituradePetrópolis #MaisHabitação #MaisVoce #Petropolis177anos #AniversariodePetropolis ",
				date: "16 de março de 2020 às 11:00",
				watched_hours: "01:00",
			},
			{
				id: "20",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/16-03%20%E2%80%93%20177%20anos%20de%20Petr%C3%B3polis.mp4",
				title: "177 anos de Petrópolis",
				author: "Prefeitura de Petrópolis",
				description: "Criada para ser a cidade de veraneio de Dom Pedro II, hoje a gente comemora os 177 anos de Petrópolis. E o que não faltam são motivos para amar cada vez mais esse lugar. Como diz o nosso hino, “Quem pensa que é feliz em outra terra é porque ainda não viveu aqui”. Feliz aniversário Petrópolis!!! #PetropolisEMais #Petropolisqueeuamo",
				date: "16 de março de 2020 às 08:00",
				watched_hours: "01:00",
			},
			{
				id: "21",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/13-03%20%E2%80%93%20Primeira%20live%20preven%C3%A7%C3%A3o%20ao%20coronav%C3%ADrus%20%28Prefeito%29.mp4",
				title: "Primeira live prevenção ao coronavírus (Prefeito)",
				author: "Prefeitura de Petrópolis",
				description: "Coletiva de Imprensa sobre a prevenção do coronavírus. #PetropolisEMais #MaisPrevenção #MaisSaude",
				date: "13 de março de 2020 às 13:13",
				watched_hours: "01:00",
			},
			{
				id: "22",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/04-03%20%E2%80%93%20Morador%20do%20Brejal%2C%20reuni%C3%A3o%20com%20Prefeito.mp4",
				title: "Morador do Brejal, reunião com Prefeito",
				author: "Prefeitura de Petrópolis",
				description: "O Morador do Brejal e componente do Comitê das estradas vicinais, Fernando Cassinelli, contou como foi a reunião com prefeito e as ações que foram acordadas para a região. #PetropolisEMais",
				date: "4 de março de 2020 às 13:12",
				watched_hours: "01:00",
			},
			{
				id: "23",
				source: "http://multiprivate-video.s3.ap-south-1.amazonaws.com/prefeitura_petropolis/03-03%20%E2%80%93%20Pista%20de%20PumpTrack.mp4",
				title: "Pista de PumpTrack",
				author: "Prefeitura de Petrópolis",
				description: "A pista de Pump Track foi inaugurada em janeiro, em Itaipava para os que amam andar de bike, e também para quem gosta de patins, skate e patinete. A pista com dois mil metros quadrados e muitas rampas radicais também estimula a prática da modalidade por atletas iniciantes. A pista foi fruto de uma parceria da prefeitura de Petrópolis com a Red Bull e LanceTracks que são parceiras do campeão mundial Henrique Avancini . 🚲🛴🎉🎊🎉 #PetropolisEMais #Maisesporte",
				date: "3 de março de 2020 às 11:56",
				watched_hours: "01:00",
			},
		];

	</script>

	<style>

		html{
			overflow: inherit;
		}

		/** Footer */
		#footer_content{
			background-color: #282828 !important;
			padding: 25px 0;
		}

		.footer{
			background-color: #282828 !important;
		}

		/** Video Gallery */

		#gallery_name{
			color: #ffffff;
			border-bottom: 1px solid;
			display: inline-block;
			padding: 10px 20px 10px 0;
		}

		#video_gallery{
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			justify-content: flex-start;
			max-height: 600px;
			overflow-y: scroll;
		}

		#video_gallery .gallery-item{
			flex: 1;
			flex-basis: 30%;
			margin: 1%;
			flex-grow: 0;
			position: relative;
		}

		#video_gallery .gallery-item video{
			width: 100%;
			height: 200px;
		}

		#video_gallery .item-information{
			display: grid;
			max-width: 85%;
		}

		#video_gallery .item-title{
			max-width: 400px;
			margin: 0;
			margin-bottom: 10px;
			width: 100%;
			max-height: 3.5em;	
			color: #ffffff;
			font-weight: bold;
			font-size: 1.2em;
			text-align: initial;
		}

		#video_gallery .item-metadata{
			text-align: initial;
			color: #c5c5c5;
			font-size: 0.85em;
			line-height: 2;
		}

		#video_gallery .item-description-wrapper{
			margin: 10px 0;
			text-align: initial;
		}

		#video_gallery .item-description{
			line-height: 1.6;
		}

		#video_gallery .description-see-more, #video_gallery .description-see-less{
			display: block;
			text-align: initial;
			text-decoration: underline;
			color: #58A4A0;
		}

		.sm-divider{
			display: none;
			margin-top: 20px;
		}

		/** Custom Scrollbar */

		#video_gallery::-webkit-scrollbar-track {
			border: 1px solid #000;
			padding: 2px 0;
			background-color: #404040;
		}

		#video_gallery::-webkit-scrollbar {
			width: 10px;
		}

		#video_gallery::-webkit-scrollbar-thumb {
			border-radius: 10px;
			box-shadow: inset 0 0 6px rgba(0,0,0,.3);
			background-color: #737272;
			border: 1px solid #000;
		}

		/** Truncate Text With Ellipsis  */

		.truncate-text{
			overflow: hidden;
			display: -webkit-box;
			-webkit-line-clamp: 3;
			-webkit-box-orient: vertical;
		}

		/** Loader */

		#loader {
			position: absolute;
			left: 50%;
			top: 50%;
			z-index: 1;
			margin: -75px 0 0 -75px;
			border: 5px solid #f3f3f3;
			border-radius: 50%;
			border-top: 5px solid #58A4A0;
			width: 100px;
			height: 100px;
			-webkit-animation: spin 2s linear infinite;
			animation: spin 2s linear infinite;
		}

		@-webkit-keyframes spin {
			0% { -webkit-transform: rotate(0deg); }
			100% { -webkit-transform: rotate(360deg); }
		}

		@keyframes spin {
			0% { transform: rotate(0deg); }
			100% { transform: rotate(360deg); }
		}

		/* Add animation to "page content" */
		.animate-bottom {
			position: relative;
			-webkit-animation-name: animatebottom;
			-webkit-animation-duration: 1s;
			animation-name: animatebottom;
			animation-duration: 1s
		}

		@-webkit-keyframes animatebottom {
			from { bottom:-100px; opacity:0 } 
			to { bottom:0px; opacity:1 }
		}

		@keyframes animatebottom { 
			from{ bottom:-100px; opacity:0 } 
			to{ bottom:0; opacity:1 }
		}

		@media(max-width: 1000px){

			#video_gallery .gallery-item {
				flex-basis: 45%;
			}			

		}

		@media(max-width: 650px){

			#video_gallery{
				justify-content: center;
			}

			#video_gallery .gallery-item {
				flex-basis: 90%;
				margin-bottom: 30px;
			}			

			.sm-divider{
				display: inherit;
			}
		}

	</style>

<?php } ?>




<style>
   .footer .footer-social-links a{
   	    font-size: 24px;
   }

   #message_twemoji{
    margin-left: -14px;
   }
</style>

</main>
</body>
</html>